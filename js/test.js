$(document).ready(function() {
    var curentuser = JSON.parse(localStorage.getItem('login'));
    $(".userdetails").text((curentuser.login.username).toUpperCase());
    if (JSON.parse(localStorage.getItem('appdata')) == null) {
        localStorage.setItem('appdata', $(".appscript").text());
    } else {
        var appret = localStorage.getItem('appdata');
        var appdetails = JSON.parse(appret);
    }
    var count = [];
    for (var n = 0; n < appdetails.apps.length; n++) {
        count[n] = 0;
    }
    $(document).on('click', ".req", function() {
        $(".fulltable").empty();
        $(".apptable").empty();
        $(".apptablbody").empty();
        $(".tablbody").empty();
        $(".schedtablbody").empty();
        $(".schedtable").empty();
        var ret = localStorage.getItem('managerdet');
        var retdet = JSON.parse(ret);
        $(".fulltable").html(
            '<tr>' +
            '<th scope="col">S.no</th>' +
            '<th scope="col">Project name</th>' +
            '<th scope="col">No.of.vacancies</th>' +
            '<th scope="col">Interview Rounds</th>' +
            '<th scope="col">Skills</th>' +
            ' <th scope="col">Job Description</th>' +
            '</tr>'
        );
        for (var i = 0; i < retdet.hmjson1.length; i++) {
            $(".tablbody").append('<tr>' +
                '<th scope="row">' + (i + 1) + '</th>' +
                '<td>' + retdet.hmjson1[i].projectname + '</td>' +
                '<td>' + retdet.hmjson1[i].vacancies + '</td>' +
                '<td>' + retdet.hmjson1[i].rounds + '</td>' +
                '<td>' + retdet.hmjson1[i].skills + '</td>' +
                '<td>' + retdet.hmjson1[i].jobdes + '</td>' +
                '</tr>')
        }
    });
    $(document).on("click", ".applications", function() {
        $(".fulltable").empty();
        $(".apptable").empty();
        $(".apptablbody").empty();
        $(".tablbody").empty();
        $(".schedtablbody").empty();
        $(".schedtable").empty();
        $(".apptable").html(
            '<tr>' +
            '<th scope="col">S.no</th>' +
            '<th scope="col">Name</th>' +
            '<th scope="col">Qualification</th>' +
            '<th scope="col">CGPA</th>' +
            '<th scope="col">Areas of Interest</th>' +
            ' <th scope="col">Experience</th>' +
            ' <th scope="col">Shortlist</th>' +
            ' <th scope="col">Tech Approved</th>' +

            '</tr>'
        );
        for (var i = 0; i < appdetails.apps.length; i++) {
            $(".apptablbody").append('<tr>' +
                '<th scope="row">' + (i + 1) + '</th>' +
                '<td>' + appdetails.apps[i]["emp" + (i)][0].appname + '</td>' +
                '<td>' + appdetails.apps[i]["emp" + (i)][0].qualification + '</td>' +
                '<td>' + appdetails.apps[i]["emp" + (i)][0].cgpa + '</td>' +
                '<td>' + appdetails.apps[i]["emp" + (i)][0].interest + '</td>' +
                '<td>' + appdetails.apps[i]["emp" + (i)][0].experience + '</td>' +
                '<td class="icon' + i + '"></td>' +
                '<td class="techcheck' + i + '"></td>' +
                '</tr>')
            if ((appdetails.apps[i]["emp" + (i)][0].tech == 1) && (appdetails.apps[i]["emp" + (i)][0].selectedapp == 1)) {
                $('.techcheck' + i).text("Yes");
            } else {
                $('.techcheck' + i).text("No");
            }
            if (appdetails.apps[i]["emp" + (i)][0].selectedapp == 1) {
                $('.icon' + i).html('<a href="javascript:void(0)" class="text-success px-2 valid validate' + i + '" value="' + i + '"><i class="fas fa-user-check "></i></a>');
            } else {
                $('.icon' + i).html('<a href="javascript:void(0)" class="text-dark px-2 valid validate' + i + '" value="' + i + '"><i class="fas fa-user-check "></i></a>');
            }
        }
    });
    $(document).on("click", ".valid", function() {
        var c = $(this).attr("value");
        for (var i = 0; i < appdetails.apps.length; i++) {
            if (i == c) {
                $('.validate' + i).removeClass("text-dark");
                $('.validate' + i).addClass("text-success");
                appdetails.apps[i]["emp" + (i)][0].selectedapp = 1;
                localStorage.setItem('appdata', JSON.stringify(appdetails));
            }
        }

    });
    $(document).on("dblclick", '.valid', function() {
        var c = $(this).attr("value");
        for (var i = 0; i < appdetails.apps.length; i++) {
            if ((i == c) && ($('.validate' + i).hasClass("text-success"))) {
                $('.validate' + i).removeClass("text-success");
                $('.validate' + i).addClass("text-dark");
                appdetails.apps[i]["emp" + (i)][0].selectedapp = 0;
                localStorage.setItem('appdata', JSON.stringify(appdetails));
            }
        }
    });
    $(document).on("click", ".schedule", function() {
        var t = 1;
        var rettech = localStorage.getItem('testMe1');
        var te = JSON.parse(rettech);
        $(".fulltable").empty();
        $(".apptable").empty();
        $(".apptablbody").empty();
        $(".tablbody").empty();
        $(".schedtablbody").empty();
        $(".schedtable").empty();
        $(".schedtable").html(
            '<tr>' +
            '<th scope="col">S.no</th>' +
            '<th scope="col">Name</th>' +
            '<th scope="col">Technical Interviewer</th>' +
            '<th scope="col"></th>' +
            '<th scope="col">Interview Date</th>' +
            '<th scope="col"></th>' +
            '<th scope="col"><button type=button class="btn-primary datebtn">Send</button></th>' +

            '</tr>'
        );
        for (var i = 0; i < appdetails.apps.length; i++) {
            if ((appdetails.apps[i]["emp" + (i)][0].tech == 1) && (appdetails.apps[i]["emp" + (i)][0].selectedapp == 1)) {
                $(".schedtablbody").append('<tr>' +
                    '<th scope="row">' + t + '</th>' +
                    '<td>' + appdetails.apps[i]["emp" + (i)][0].appname + '</td>' +
                    '<td ><select class=" sel myselect' + i + '">' +
                    '</select></td>' +
                    '<td></td>' +
                    '<td><input type="date" class="date' + i + '"></input></td>' +
                    '<td>' + appdetails.apps[i]["emp" + (i)][0].date + '</td>' +
                    '<td></td>' +
                    '</tr>')
                t++;
                for (var j = 0; j < te.logs[0].employee0.length; j++) {
                    $('.myselect' + i).append('<option value="' + (j + 1) + '">' + te.logs[0].employee0[j].name + '</option>');
                }

            }

        }
    });

    function addClickHandler(i) {
        $(document).on('change', '.myselect' + i, function() {
            appdetails.apps[i]["emp" + (i)][0].techperson = ($('.myselect' + i).val());
            localStorage.setItem('appdata', JSON.stringify(appdetails));
        });
    }
    var j;
    for (j = 0; j < appdetails.apps.length; j++) {
        if (appdetails.apps[j]["emp" + (j)][0].tech == 1) {
            addClickHandler(j);
        }
    }

    function adddate(i) {
        $(document).on('change', '.date' + i, function() {
            var today = new Date();
            var assignDate = new Date($('.date' + i).val());
            var todayms = today.getTime();
            var bms = assignDate.getTime();
            if (bms < todayms) {
                alert("Enter a valid date for scheduling");
            }
            appdetails.apps[i]["emp" + (i)][0].date = ($('.date' + i).val());
            localStorage.setItem('appdata', JSON.stringify(appdetails));

        });
    }
    var k;
    for (k = 0; k < appdetails.apps.length; k++) {
        if (appdetails.apps[k]["emp" + (k)][0].tech == 1) {
            adddate(k);
        }
    }
    $(document).on("click", ".cand", function() {
        $(".fulltable").empty();
        $(".apptable").empty();
        $(".apptablbody").empty();
        $(".tablbody").empty();
        $(".schedtablbody").empty();
        $(".schedtable").empty();
        $(".schedtable").html(
            '<tr>' +
            '<th scope="col">S.no</th>' +
            '<th scope="col">Name</th>' +
            '</tr>'
        );
        var s = 1;
        for (var i = 0; i < appdetails.apps.length; i++) {
            if ((appdetails.apps[i]["emp" + (i)][0].res == 1) && (appdetails.apps[i]["emp" + (i)][0].tech == 1) && (appdetails.apps[i]["emp" + (i)][0].selectedapp == 1)) {
                $(".schedtablbody").append('<tr>' +
                    '<th scope="row">' + s + '</th>' +
                    '<td>' + appdetails.apps[i]["emp" + (i)][0].appname + '</td>' +
                    '</tr>')
                s++;
            }
        }
    });
    $(document).on("click", ".datebtn", function() {
        var appret1 = localStorage.getItem('appdata');
        var appdetails1 = JSON.parse(appret1);
        for (var m = 0; m < appdetails1.apps.length; m++) {
            for (var l = 0; l < (appdetails1.apps.length); l++) {
                if ((appdetails1.apps[m]["emp" + (m)][0].date == appdetails1.apps[l]["emp" + (l)][0].date)) {
                    count[m] = count[m] + 1;
                }
            }
            if (count[m] > 6) {
                alert("already 6 candidates have been scheduled for" + appdetails1.apps[m]["emp" + (m)][0].date);
                break;
            }
        }

    });
});