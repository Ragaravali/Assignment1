$(document).ready(function() {
    var empappdetails = localStorage.getItem('appdata');
    var empapp = JSON.parse(empappdetails);
    var retrievedObject = localStorage.getItem('testMe1');
    var json1 = JSON.parse(retrievedObject);
    $(document).on('click', '.selapp', function() {
        $(".fulltable").empty();
        $(".tablbody").empty();
        $(".apptable").empty();
        $(".apptablbody").empty();
        var t = 1;
        for (var i = 0; i < empapp.apps.length; i++) {
            if (empapp.apps[i]["emp" + (i)][0].selectedapp == 1) {
                $(".apptable").html(
                    '<tr>' +
                    '<th scope="col">S.no</th>' +
                    '<th scope="col">Name</th>' +
                    '<th scope="col">Qualification</th>' +
                    '<th scope="col">CGPA</th>' +
                    '<th scope="col">Areas of Interest</th>' +
                    ' <th scope="col">Experience</th>' +
                    ' <th scope="col">Shortlist</th>' +
                    '</tr>'
                );

                $(".apptablbody").append('<tr>' +
                    '<th scope="row">' + t + '</th>' +
                    '<td>' + empapp.apps[i]["emp" + (i)][0].appname + '</td>' +
                    '<td>' + empapp.apps[i]["emp" + (i)][0].qualification + '</td>' +
                    '<td>' + empapp.apps[i]["emp" + (i)][0].cgpa + '</td>' +
                    '<td>' + empapp.apps[i]["emp" + (i)][0].interest + '</td>' +
                    '<td>' + empapp.apps[i]["emp" + (i)][0].experience + '</td>' +
                    '<td class="icon' + i + '"></td>' +
                    '</tr>')
                t++;
                if (empapp.apps[i]["emp" + (i)][0].tech == 1) {
                    $('.icon' + i).html('<a href="javascript:void(0)" class="text-success px-2 valid validate' + i + '" value="' + i + '"><i class="fas fa-user-check "></i></a>');
                } else {
                    $('.icon' + i).html('<a href="javascript:void(0)" class="text-dark px-2 valid validate' + i + '" value="' + i + '"><i class="fas fa-user-check "></i></a>');
                }

            }
        }

    });
    $(document).on("click", ".valid", function() {
        var c = $(this).attr("value");
        for (var i = 0; i < empapp.apps.length; i++) {
            if (i == c) {
                $('.validate' + i).removeClass("text-dark");
                $('.validate' + i).addClass("text-success");
                empapp.apps[i]["emp" + (i)][0].tech = 1;
                localStorage.setItem('appdata', JSON.stringify(empapp));
            }
        }

    });
    $(document).on("dblclick", '.valid', function() {
        var c = $(this).attr("value");
        for (var i = 0; i < empapp.apps.length; i++) {
            if ((i == c) && ($('.validate' + i).hasClass("text-success"))) {
                $('.validate' + i).removeClass("text-success");
                $('.validate' + i).addClass("text-dark");
                empapp.apps[i]["emp" + (i)][0].tech = 0;
                localStorage.setItem('appdata', JSON.stringify(empapp));
            }
        }
    });
    $(document).on("click", ".sched", function() {
        var login = JSON.parse(localStorage.getItem('login'));
        $(".tablbody").empty();
        $(".apptable").empty();
        $(".apptablbody").empty();
        for (var i = 0; i < empapp.apps.length; i++) {
            for (var j = 0; j < json1.logs[0].employee0.length; j++) {
                if (empapp.apps[i]["emp" + (i)][0].techperson == json1.logs[0].employee0[j].id) {
                    if (json1.logs[0].employee0[j].name == login.login.username) {
                        $(".fulltable").empty();


                        $(".fulltable").html(
                            '<tr>' +
                            '<th scope="col">Name</th>' +
                            '<th scope="col">Date of Interview</th>' +
                            '<th scope="col">Score</th>' +
                            '<th scope="col">Result</th>' +
                            '</tr>'
                        );
                        $(".tablbody").append('<tr>' +

                            '<td>' + empapp.apps[i]["emp" + (i)][0].appname + '</td>' +
                            '<td>' + empapp.apps[i]["emp" + (i)][0].date + '</td>' +
                            '<td><input type="text" class="score' + i + '"></input></td>' +
                            '<td class="result' + i + '"></td>' +
                            '</tr>')
                        if (empapp.apps[i]["emp" + (i)][0].res == 1) {
                            $(".rescheck").removeClass("resfail")
                            $(".result" + i).html('<i class="fas fa-check text-success rescheck"></i>');
                        } else {
                            $(".rescheck").removeClass("rescheck");
                            $(".result" + i).html('<i class="fas fa-times text-danger resfail"></i>');
                        }

                    }
                }
            }
        }
    });

    function addscore(i) {
        $(document).on('input', '.score' + i, function() {
            if ($('.score' + i).val() >= 3) {
                $(".rescheck").removeClass("resfail");
                $(".result" + i).html('<i class="fas fa-check text-success rescheck"></i>');
                empapp.apps[i]["emp" + (i)][0].res = "1";
                localStorage.setItem('appdata', JSON.stringify(empapp));

            } else {
                $(".rescheck").removeClass("rescheck");
                $(".result" + i).html('<i class="fas fa-times text-danger resfail"></i>');
                empapp.apps[i]["emp" + (i)][0].res = "0";
                localStorage.setItem('appdata', JSON.stringify(empapp));
            }
        });
    }
    for (var k = 0; k < empapp.apps.length; k++) {

        addscore(k);
    }
});