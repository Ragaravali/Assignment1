$(document).ready(function() {
    var count = 0;
    var ret = localStorage.getItem('testMe1');
    var regret = JSON.parse(ret);
    var $regpass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*()_+-=:;""'<>?/\|'])[0-9a-zA-Z~!@#$%^&*()_+-=:;""'<>?/\|']{8,}$/;

    $("#name1").focus(function() {
        $("#name1").addClass("border-danger");
        $("#name1").removeClass("border-success");
    });
    $("#name1").on("input", function() {
        if ($(this).val($(this).val().replace(/[0-9-!@#$%^&*?+()]/g, ''))) {

            $("#name1").removeClass("border-danger");
            $("#name1").addClass("border-success");
        }
        button1();
    });
    $("#passwd").focus(function() {
        $("#passwd").addClass("border-danger");
        $("#passwd").removeClass("border-success");
    });
    $("#passwd").on("input", function() {
        var a = $(this);
        if (!a.val().match($regpass)) {
            $("#passerr").text('password  have atleast one small,one cap,one number');
            $("#passwd").addClass("border-danger");

        } else {
            $("#passerr").text('');
            $("#passwd").removeClass("border-danger");
            $("#passwd").addClass("border-success");

        }
        button1();
    });
    $(document).on("click", ".regbut", function() {
        var x = $('input[name="role"]:checked').val();
        switch (x) {
            case "0":
                {
                    regret.logs[0].employee0.push({
                        "id": regret.logs[0].employee0.length + 1,
                        "name": $("#name1").val(),
                        "password": $('#passwd').val()
                    })
                    localStorage.setItem('testMe1', JSON.stringify(regret));
                    break;
                }
            case "1":
                {
                    regret.logs[1].employee1.push({
                        "id": regret.logs[1].employee1.length + 1,
                        "name": $("#name1").val(),
                        "password": $('#passwd').val()
                    })
                    localStorage.setItem('testMe1', JSON.stringify(regret));
                    break;
                }
            case "2":
                {
                    regret.logs[2].employee2.push({
                        "id": regret.logs[2].employee2.length + 1,
                        "name": $("#name1").val(),
                        "password": $('#passwd').val()
                    })
                    localStorage.setItem('testMe1', JSON.stringify(regret));
                    break;
                }
        }
    });

    function button1() {
        $("form").find(".border-success").each(function() {
            count++;
        });

        count == 2 ? $("button[type=button]").prop('disabled', false) : $("button[type=button]").prop('disabled', true);
        count = 0;

    }
    $(document).on("click", ".regbut", function() {
        window.location.href = "test.html";
    });
});