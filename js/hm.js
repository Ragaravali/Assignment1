$(document).ready(function() {
    var curentuser = JSON.parse(localStorage.getItem('login'));
    $(".userdetails").text((curentuser.login.username).toUpperCase());
    var appret = localStorage.getItem('appdata');
    var appdetails = JSON.parse(appret);
    if (JSON.parse(localStorage.getItem('managerdet')) == null) {
        localStorage.setItem('managerdet', $("#hmjson").text());

    } else {
        var ret = localStorage.getItem('managerdet');
        var retdet = JSON.parse(ret);
        $(document).on('click', ".hmsend", function() {
            if ($(".proname").val() == null || $(".vaca").val() == null || $('input[name=intround]:checked').val() == null || $(".mulsel").val() == null || $(".jd").val() == null) {
                alert("Fill all the mandatory fields");
            } else {
                var ret = localStorage.getItem('managerdet');
                var retdet = JSON.parse(ret);
                retdet.hmjson1.push({
                    "projectname": $(".proname").val(),
                    "vacancies": $(".vaca").val(),
                    "rounds": $('input[name=intround]:checked').val(),
                    "skills": $(".mulsel").val(),
                    "jobdes": $(".jd").val()
                })
                localStorage.setItem('managerdet', JSON.stringify(retdet));
            }
        });
        $(document).on("click", ".jobsuccess", function() {
            $(".fulltable").empty();
            $(".fulltablbody").empty()
            $(".fulltable").html(
                '<tr>' +
                '<th scope="col">S.no</th>' +
                '<th scope="col">Name</th>' +
                '</tr>'
            );
            var s = 1;
            for (var i = 0; i < appdetails.apps.length; i++) {
                if (appdetails.apps[i]["emp" + (i)][0].res == 1) {
                    $(".fulltablbody").append('<tr>' +
                        '<th scope="row">' + s + '</th>' +
                        '<td>' + appdetails.apps[i]["emp" + (i)][0].appname + '</td>' +
                        '</tr>')
                    s++;
                }
            }
        });
    }
});